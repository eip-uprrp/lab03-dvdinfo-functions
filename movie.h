#ifndef MOVIE_H
#define MOVIE_H

#include <string>
#include <QDebug>
#include "filemanip.h"
#include <QString>
#include <QStringList>
#include <iostream>

using namespace std ;

// Given a movie's name and the DVD database file reference,
// returns the movie's info string.
string getMovieByName(string moviename, filemanip &file) ;

// Given a movie's position and the DVD database file reference,
// returns the movie's info string.
string getMovieByPosition(int position, filemanip &file) ;

// Given the DVD database file reference and a range of positions
// specified by parameters <start> and <end>, displays information for 
// the range of movies
void showMovies(filemanip &file, int start=1, int end=10) ;

// Given the DVD database file reference and a keyword,
// displays all movies whose names contain the keyword
void showMovies(filemanip &file, string keyword) ;

// Given a movie's info string, displays the name, rating, 
// genre, and year.
void showMovie(string movieinfo) ;

// Given a movie's info string, returns the movie's name.
string getMovieName(string movieinfo) ;

// Given a movie's info string, returns the movie's rating.
string getMovieRating(string movieinfo) ;

// Given a movie's info string, returns the movie's year.
string getMovieYear(string movieinfo) ;

// Given a movie's info string, returns the movie's genre.
string getMovieGenre(string movieinfo) ;

// Given a movie's info string, returns (by reference) 
// the movie's name, rating, year, and genre.
void getMovieInfo(string movieinfo, string &name, string &rating, string &year,
                  string &genre);


#endif // MOVIE_H
