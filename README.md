#Lab. 3: Utilizando funciones en C++

<div align='center'><img src="http://i.imgur.com/3evSXAP.png?2" width="215" height="174">  <img src="http://i.imgur.com/WXYnAOq.png?1" width="215" height="174">  <img src="http://i.imgur.com/bu6VkBT.png?1" width="215" height="174"></div>

<p> </p>

Una buena manera de organizar y estructurar los programas de computadoras es dividiéndolos en partes más pequeñas utilizando funciones. Cada función realiza una tarea específica del problema que estamos resolviendo.

Hemos visto que todos los programas en C++ deben contener la función `main` que es donde comienza el programa. También hemos utilizado las funciones `pow`, `sin` y `cos`  de la biblioteca de matemática `cmath`. Dado que en casi todas las experiencias de laboratorio que siguen estaremos utilizando funciones que ya han sido creadas, necesitamos aprender cómo trabajar con ellas. Más adelante aprenderemos cómo diseñarlas y validarlas.

##Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán reforzado su conocimiento sobre los elementos esenciales de la definición de una función en C++: tipo, nombre, lista de parámetros y cuerpo de la función. También habrán practicado el invocar funciones ya creadas enviando valores para los parámetros ("pass by value") y referencias para los parámetros ("pass by reference").  

##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado 

	a. los elementos básicos de la definición de una función en C++

	b. la manera de invocar funciones en C++

	c. la diferencia entre  parámetros pasados por valor y por referencia

	d. como devolver el resultado de una función.


2. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

3. haber tomado el [quiz Pre-Lab 3](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7119) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).



##Funciones


En matemática, una función $f$ es una regla que se usa para asignar a cada elemento $x$ de un conjunto que llamamos *dominio*, uno (y solo un) elemento $y$ de un conjunto que llamamos *campo de valores*. Muchas veces representamos esa regla como una ecuación $y=f(x)$. La variable $x$ es el parámetro de la función y la variable $y$ contendrá el resultado. Una función puede tener más de un parámetro pero solo un resultado. Por ejemplo, podemos tener funciones de la forma $y=f(x_1,x_2)$ en donde hay dos parámetros y para cada par $(a,b)$ que se use como argumento de la función, la función tiene un solo valor de $y=f(a,b)$. El dominio de la función nos dice el tipo de valor que debe tener el parámetro y el campo de valores el tipo de valor que tendrá el resultado.

Las funciones en programación de computadoras son similares. Una función 
tiene una serie de instrucciones que toman los valores asignados a los parámetros y realiza alguna tarea. La única diferencia es que una función en programación puede no devolver valor (en este caso la función se declara `void`). Si la función va a devolver algún valor, se hace con la instrucción `return`. Al igual que en matemática especificamos el dominio y el campo de valores, en programación debemos especificar los tipos de valores que tienen los parámetros y el resultado; esto se hace al declarar la función.

###Encabezado de una función:

La primera oración de una función se llama el *encabezado* y su estructura es como sigue:

`tipo nombre(tipo parámetro01, ..., tipo parámetro0n)`

Por ejemplo,

`int ejemplo(int var1, float var2, char &var3)`

sería el encabezado de la función llamada `ejemplo`, que devuelve un valor entero. La función recibe como argumentos un valor entero (y guardará una copia en `var1`), un valor "float" (y guardará una copia en `var2`) y la referencia a una variable de tipo  `char` que se guardará en la variable de referencia `var3`. Nota que `var3` tiene un signo de `&` antes del nombre de la variable. Esto indica que `var3` contendrá la referencia a un caracter.

###Invocación

Si queremos guardar el valor del resultado de la función `ejemplo` en la variable `resultado` (que deberá ser de tipo entero), invocamos la función enviando argumentos de manera similar a:

`resultado=ejemplo(2, 3.5, unCar);`

Nota que no se incluye el tipo de las variables en la invocación. Como en la definición de la función `ejemplo` el tercer parámetro `&var3` es una variable de referencia, lo que se está enviando en el tercer argumento de la invocación es una *referencia* a la variable `unCar`. Los cambios que se hagan en la variable `var3` están cambiando el contenido de la variable `unCar`.

También puedes usar el resultado de la función sin tener que guardarlo en una variable. Por ejemplo puedes imprimirlo:

`cout << "El resultado de la función ejemplo es:" << ejemplo(2, 3.5, unCar);`

o utilizarlo en una expresión aritmética:

`y=3 + ejemplo(2, 3.5, unCar);`



###Funciones sobrecargadas (‘overloaded’)

Las funciones sobrecargadas son funciones que poseen el mismo nombre, pero *firma* diferente.

La firma de una función se compone del nombre de la función, y los tipos de parámetros que recibe, pero no incluye el tipo que devuelve.

Los siguientes prototipos de funciones tienen la misma firma:

```
int ejemplo(int, int) ;
void ejemplo(int, int) ; 
string ejemplo(int, int) ;
```

Nota que todas tienen el mismo nombre, `ejemplo`, y reciben la misma cantidad de parámetros del mismo tipo `(int, int)`.

Los siguientes prototipos de  funciones tienen firmas diferentes:

```
int ejemplo(int) ;
int olpmeje(int) ;
```
Nota que a pesar de que las funciones tienen la misma cantidad de parámetros con mismo tipo `int`, el nombre de las funciones es distinto.

Los siguientes prototipos de funciones son versiones sobrecargadas de la función `ejemplo`:

```
int ejemplo(int) ;
void ejemplo(char) ;
int ejemplo(int, int) ;
int ejemplo(char, int) ;
int ejemplo(int, char) ;
```

Todas las funciones de arriba tienen el mismo nombre, `ejemplo`, pero distintos parámetros.  La primera y segunda función tienen la misma cantidad de parámetros, pero los argumentos son de distintos tipos.  La cuarta y quinta función tienen argumentos de tipo char e int, pero en cada caso están en distinto orden.

En ese último ejemplo la función ejemplo es sobrecargada ya que hay 5 funciones con firma distinta pero con el mismo nombre.


###Valores por defecto

Se pueden asignar valores por defecto (‘default’) a los parámetros de las funciones desde el parámetro más a la derecha, hasta el más a la izquierda, sin dejar parámetros entre medio sin inicializar. Esto permite la invocación de la función sin tener que enviar valores en la posición de este parámetro.

**Ejemplos de encabezados de funciones e invocaciones válidas:**

1. **Encabezado:** `int ejemplo(int var1, float var2, int var3 = 10)` Aquí se inicializa `var3` a 10.

     **Invocaciones:** 
    a. `y = ejemplo(5, 3.3, 12)` Esta invocación asigna el valor 5 a `var1`, el valor 3.3 a `var2`, y el valor de `var3` cambia a 12. 

    b. `y =ejemplo(5, 3.3)`  Esta invocación envía valores para los primeros dos parámetros y el valor del último parámetro será el valor por defecto asignado en el encabezado. Esto es, los valores de las variables en la función serán: `var1` tendrá 5, `var2` tendrá 3.3 y `var3` tendrá 10.

2. **Encabezado:** `int ejemplo(int var1, float var2=5.0, int var3 = 10)`  Aquí se  inicializa `var2` a 5 y `var3` a 10.
    
    **Invocaciones:** 
    a. `y = ejemplo(5, 3.3, 12)` Esta invocación asigna el valor 5 a `var1`, el valor 3.3 a `var2`, y el valor 12 a  `var3`. 

    b. `y = ejemplo(5, 3.3)` En esta invocación solo se envían valores para los primeros dos parámetros, y el valor del último parámetro es el valor por defecto.  Esto es, el valor de `var1` dentro de la función será 5, el de `var2` será 3.3 y el de `var3` será 10.

    c. `y = ejemplo(5)` En esta invocación solo se envía valor para el primer parámetro, y los últimos dos parámetros tienen valores por defecto.  Esto es,  el valor de `var1` dentro de la función será 5, el de `var2` será 5.0 y el de `var3` será 10.


**Ejemplo de un encabezado de funciones válido con invocación inválida:**

1. **Encabezado:** `int ejemplo(int var1, float var2=5.0, int var3 = 10)`  Aquí se  inicializa `var2` a 5 y `var3` a 10.

    **Invocación:** `y = ejemplo(5, ,10)` Esta invocación  es **inválida** porque   deja espacio vacío en el argumento del medio. 

**Ejemplos de un encabezado de funciones inválidos:**

1. `int ejemplo(int var1=1, float var2, int var3)` Este encabezado es inválido porque los valores por defecto solo se pueden asignar desde el parámetro más a la derecha.

2. `int ejemplo(int var1=1, float var2, int var3=10)`: Este encabezado es inválido porque no se pueden poner parámetros sin valores en medio de parámetros con valores por defecto. En este caso  `var2` no tiene valor pero `var1` y `var3` si.



##Películas DVD y base de datos DVD

DVD son las siglas para “digital versatile disk” o “digital video disk” que en español significa disco versátil digital o disco de video digital. Este  es un formato de disco óptico para almacenamiento digital  inventado por Philips, Sony, Toshiba, y Panasonic en 1995.  Los DVD ofrecen capacidad de almacenamiento mayor que los discos compactos (CD), pero tienen las mismas dimensiones.  Los DVD pueden ser utilizados para almacenar cualquier dato digital, pero son famosos por su uso en la distribución de películas en los hogares.




## Sesión de laboratorio

En este laboratorio vamos a utilizar una base de datos de películas DVD mantenida por http://www.hometheaterinfo.com/dvdlist.htm que contiene 44MB de información de películas que han sido distribuidas en DVD. Alguna de la información almacenada en esta base de datos es: título del DVD, estudio de publicación, fecha de publicación, tipo de sonido, versiones, precio, clasificación, año y  género.  Los campos de la información de cada película son almacenados en texto con el siguiente formato:


`DVD_Title|Studio|Released|Status|Sound|Versions|Price|Rating|Year|Genre|Aspect|UPC|DVD_ReleaseDate|ID|Timestamp`

###Instrucciones:

1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab03-DVDInfo-Functions.git` para descargar la carpeta `Lab03-DVDInfo-Functions` a tu computadora.

2.  Haz doble "click" en el archivo `DVDInfo.pro` para cargar este proyecto a Qt. 

3. El archivo `main.cpp` tiene la invocación de las funciones en los siguientes ejercicios.  En los archivos `movie.h` y `movie.cpp` se encuentra la declaración y definición de las funciones que vamos a invocar.
 
###**Ejercicio 1**

Para el ejercicio 1 marque (doble-click) el archivo `movie.h`

El archivo `movie.h` contiene los prototipos de las funciones de este proyecto. Ve a `movie.h` e identifica cuál o cuáles funciones son sobrecargadas y describe por qué.

Estudia los prototipos de funciones contenidas en `movie.h` de modo que sepas la tarea que realizan y los tipos de datos que reciben y devuelven. Identifica los tipos de datos que recibe y devuelve cada una de las siguientes funciones:

```
showMovie
showMovies (las dos)
getMovieName
getMovieByName
```

En el archivo `movie.cpp` encontrarás las definiciones de las funciones. Nota que la función `showMovie` usa el objeto `fields` que es una instancia de la clase `QStringList`. Cada campo de información de la película es una entrada `fields[i]` de la lista `fields`. El primer campo corresponde al título de la película y está en la entrada `fields[0]`.


###**Ejercicio 2**


1. Ve al archivo `main.cpp` y modifica la función `main` para que despliegue en la pantalla las películas en las posiciones 80 hasta la 100.


2. Ahora modifica  la función `main` para que despliegue en la pantalla solo las películas que contengan “forrest gump” en el título.


3. Modifica nuevamente la función `main` para que despliegue en la pantalla solo la película en la posición  75125 usando composición de funciones y la función `showMovie`.

4. Para la película en la parte  3 de este ejercicio, modifica la función `main` para que solo despliegue el nombre y el rating de la película.

5. Para la película en la parte 3, modifica la función `main` para que despliegue el nombre, el rating, el año y el género de la película en una sola línea. Ayuda: nota que la función `getMovieInfo` tiene parámetros por referencia.



###**Ejercicio 3**

Las funciones cuyos prototipos están en `movie.h` están implementadas en el archivo `movie.cpp`. En este ejercicio  vas a utilizar los archivos `movie.h`, `movie.cpp`, y `main.cpp` para definir e implementar funciones adicionales. Estudia las funciones que ya están implantadas en `movie.cpp` para que te sirvan de ejemplo para las funciones que vas a crear. 

1. Implementa una función `getMovieStudio` que reciba una cadena de caracteres con la info de una película y devuelva el nombre del estudio de la película. Recuerda añadir el prototipo de la función en el archivo `movie.h`. Invoca la función `getMovieStudio` desde `main()` para desplegar el nombre y el estudio de la película en la posición 75125 y así demostrar su funcionamiento.

2. Implementa una función sobrecargada `getMovieInfo` que devuelva el nombre del estudio además del nombre, rating, año y género. Invoca la función `getMovieInfo` desde `main()` para desplegar el nombre,  estudio, rating, año y género de la película en la posición 75125 y así demostrar su funcionamiento.


3. Implementa una función `showMovieInLine` que **despliegue** la información de una película que despliega `showMovie` pero en una sola línea. La función debe tener un parámetro de modo que reciba string de información de la película. Invoca la función `showMovieInLine` desde `main()` para desplegar la información de la película en la posición 75125 y así demostrar su funcionamiento.

4. Implementa una función `showMoviesInLine` que **despliegue** la misma información que despliega `showMovies`  (todas las películas en un rango de posiciones) pero en una sola línea. Por ejemplo, una invocación a la función sería
`showMoviesInLine(file, 148995, 149000);`. Invoca la función `showMoviesInLine` desde `main()` para demostrar su funcionamiento.

##Entregas

1. Usando Moodle, entrega un archivo que incluya la función `main()` que modificaste en el Ejercicio 2 y las funciones `getMovieStudio`, `getMovieInfo`, `showMovieInLine`, `showMoviesInLine` que implementaste en el Ejercicio 3.




