#ifndef FILEMANIP_H
#define FILEMANIP_H
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QDebug>

class filemanip
{
public:
    filemanip() ;
    filemanip(QString filename);
    QString getnext() ;
    void reset() ;
    ~filemanip() ;
private:
    QFile *file ;
    QTextStream *in ;
};

#endif // FILEMANIP_H
