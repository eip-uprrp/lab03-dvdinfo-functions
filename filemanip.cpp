#include "filemanip.h"

filemanip::filemanip()
{
    filemanip(":/dvd_csv.txt") ;
}

filemanip::filemanip(QString filename){
    file = new QFile(filename);
    file->open(QIODevice::ReadOnly) ;
    if(file)
        in = new QTextStream(file) ;
    else
        qDebug("Cant open!") ;

}

QString filemanip::getnext(){
    if(!in->atEnd())
           return in->readLine() ;
    return "" ;
}

filemanip::~filemanip(){
    delete in ;
    file->close() ;
    delete file ;
}

void filemanip::reset(){
    in->seek(0) ;
}
